let theHost = 'https://gitlab.com';

const init = function(){
    $('body').on('click', 'li.board-card', function(event){        
        if (!isALinkElement(event.target)) {
            let url = theHost + $(this).find('a').attr('href');
            showIssueDescription(url);    
        }
        
    });
}

const isALinkElement = function(element) {
    return $(element).is("a");   
}

const showIssueDescription = function(url) {
    $.get(url, function(data) {
        let issueDescription = parseIssueDescription(data);         
        
        let content = buildDescriptionHtml(issueDescription);
        
        showModal(content);
    });
}

const parseIssueDescription = function(content) {
    let dom = $('<div/>').html(content).contents();
    return dom.find('.detail-page-description').html();
}

const buildDescriptionHtml = function(description) {
    let modalContent = `<div class="detail-page-description content-block">${description}</div>`;
    let html = `<div class="gitlab-issue-preview">${modalContent}</div>`;
    return html;
}

const showModal = function(content) {
    $(content).appendTo('body').modal();

    // the jquery-modal plugin adds a .modal class to our container, 
    // we should remove that class because it conflicts with gitlab's own stylesheet
    $('.gitlab-issue-preview').removeClass('modal');
}

$(function() {
    init(); 
});