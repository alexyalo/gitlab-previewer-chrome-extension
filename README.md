# What is this?
This is a Google Chrome extension that helps developers who use Gitlab issue boards. Doesn't it annoy you that the board wont let you see an issue's description unless you click on the title and navigate to the issue detail page? If you just want to see the issue's description without leaving the board you can't. 

So, I built this basic extension that when you click on an issue card on the Gitlab board it will fetch the description and show it to you on a modal without having to navigate out of the board.
Feel free to fork the repo and customize it.

![Preview](preview.gif)

[Check the article I wrote on the process of building this](https://medium.com/alex-yalo/how-i-improved-my-experience-with-gitlab-boards-building-a-chrome-extension-170bd31e3742)

# How to install
* Clone this repo in a local folder
* In Google Chrome go to this address chrome://extensions/ or to the Context Menu -> More Tools -> Extensions
* Enable "Developer Mode" (top right)
* Click on "Load Unpacked" button
* Select the folder where you cloned the repo
* That's it! Go to your Gitlab board, refresh the page and try clicking on an issue card

# Known issues
* If there's an image in the issue description, it won't render into the modal provided by this extension.